@extends('layouts.app')

@section('title', 'Users')

@section('content')

@csrf

@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
@if(Session::has('success'))
<div class = 'info'>
    {{Session::get('success')}}
</div>
@endif
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Role</th><th>Make manager</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>
            @if(auth()->user()->isAdmin())
            <div class="form-group">
            
                <select class="form-control" name="department_id">                                                                         
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}"> 
                            {{ $department->name }} 
                        </option>
                    @endforeach    
                           
                    </select>
                    <input type = "submit" name = "submit" value = " Update Department">
        </div>
        @else
        {{$user->department->name}}
        @endif

        </td>       
            <td>
                @foreach($user->roles as $role)
                    <div>{{ $role->name }}</div>
                @endforeach
            </td>
           
           
            <td>
                @if(!$user->isManager())
                    <a href = "{{route('user.makemanager',$user->id)}}">make manager</a>
                    @else
                    <a href = "{{route('user.removemanager',$user->id)}}">remove manager</a>
                @endif
            </td> 


            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td>
                <a href =  "{{route('users.show',$user->id)}}"> Details</a>
            </td>
            <td>
            
                <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            
                
            </td> 

        </tr>
    @endforeach
</table>
@endsection

