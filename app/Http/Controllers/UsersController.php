<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\Role;
use App\Department;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $departments = Department::all();        
        return view('users.index', compact('departments','users'));
    }

    public function changeDepartment($uid, $did = null){
       // Gate::authorize('assign-user');
        $user = User::findOrFail($uid);
        $user->department_id = $did;
        $user->save(); 
        return back();
        //return redirect('candidates');
        //comment
    }

    public function makeManager($uid)
    {
        $user = User::findOrFail($uid);
        if(Gate::allows('make-manager'))
        {
            $managerrole = Role::where('name','manager')->first();
            $user->roles()->attach($managerrole);
            $user->save();
            Session::flash('success', 'Changed!');
        }else{
            Session::flash('notallowed', 'You are not allowed to change the role of the user becuase you are not Admin');
        }
        return redirect('users');
    }   

    public function removeManager($uid)
    {
        $user = User::findOrFail($uid);
        if(Gate::allows('make-manager'))
        {
            $managerrole = Role::where('name','manager')->first();
            $user->roles()->detach($managerrole);
            $user->save();
            Session::flash('success', 'Changed!');
        }else{
            Session::flash('notallowed', 'You are not allowed to change the role of the user becuase you are not Admin');
        }
        return redirect('users');
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new user();
                //$candidate->name = $request->name; 
                //$candidate->email = $request->email;
                $use = $user->create($request->all());
                $use->save();
                return redirect('users');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->user()->isAdmin())
        {
            $user = User::findOrFail($id);
            $user->delete(); 
            return redirect('users'); 
        }
        return back();
    }
}
